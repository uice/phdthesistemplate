An unofficial LaTeX class for University of Iceland Ph.D. theses
================================================================

Version 2.1.0 2022-04-06

End user relevant changes from v2.0.0 to v2.1.0:
----------------------------------------------
1. Usage of `natbib` has been removed from `.cls` and it is rather left to the user to decide in the `.tex` file what bibliography system to use. The new `uiphd_template.tex` provides an example using BibLaTeX.


End user relevant changes from v0.1 to v2.0.0:
----------------------------------------------
1. replace the old `ui-phdthesis.cls` by the new one and
2. replace the two files `HIlogo.pdf` and `UIblueribbon.pdf` by `banner.png`
3. add `\thesislicense{All rights reserved}` to your `.tex` file (or update to the license you want to make your thesis available).
4. Also check the comments at the start of file `uiphd_template.tex` for possible further additions to your existing `.tex` file (regarding `\numberwithin` and `\UrlBreaks`).

See Releases here in GitLab for detailed changelog.

Use the Issue tracker here in GitLab for comments.

You are welcome to contribute changes back (e.g. via fork and merge requests or simply a diff).
