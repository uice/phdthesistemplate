%
% An unofficial LaTeX class for University of Iceland Ph.D thesis
% patterned after rules for Ph.D. thesis
% in the School of Engineering and Natural Sciences

%
% Creator and maintainer Einar Arnason, einararn@hi.is
% 23-10-2014 version 0.1
% Copyright (C) 2014 Einar Arnason
% Permission is granted to copy, distribute and/or modify this document
% under the terms of the GNU Free Documentation License, Version 1.3
% or any later version published by the Free Software Foundation;
% with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
% A copy of the license is available at http://www.gnu.org/
%
% 06-04-2022 version 2.1.0
% https://gitlab.com/uice/phdthesistemplate
% Helmut Neukirchen https://uni.hi.is/helmut
% Improvements are welcome
% tikzpicture-based approach by Þór Arnar Curtis for the title page using the new HÍ 2021 design 

% When including papers using \insterpdffile, run them first through PDFcrop
% https://github.com/ho-tex/pdfcrop
% and if the pages have different size, e.g. last page of a two column
% paper uses only one column, take care to use the same cropping for
% all pages. The PDFcrop is needed because typically the original PDF
% (e.g. your camera-ready version of your paper) contains, e.g. an A5
% sized paper on A4 paper format and since the thesis is printed in
% A5, the paper gets scaled down so that the original A5-sized paper
% becomes a tiny A6 which is too tiny!

% TODO/Future improvements:
% - Use a vector graphic format for the logo on the title page.
% - Add support for thesis subtitle (as supported by the MSc thesis template)
% -  Currently, US English is used (e.g. US "fulfillment" instead of UK "filfillment" and
%   "Acknowledgments" instead of UK "Acknowledgements" and harcoded [icelandic,american]{babel}, but
%   it would be nice to support this is a package option (or keep the bablte outside the package and
%   use rather the language set by the user via babel).
% - Upload to, e.g. B2SHARE or Zenodo to long-term archive it.

% Changelog:
% V2.1.0:
% - Added bookmark package and \phantomsection commands (needed for starred sections) to make the PDF bookmarks work
% - Remove natbib from *.cls file. Added BibLaTex to *.tex template
% - Start using https://gitlab.com/uice/phdthesistemplate
% V2.0.0:
% - Updated to new HÍ 2021 design
% - Added \thesislicense to set the license (e.g. to support creative commons)
% - Updated wording ("Advisor"->"Supervisor", "PhD Committee"->"Doctoral Commitee") based on
% Regulation no. 995-2017 on doctoral study at the School of Engineering and Natural Sciences
% https://english.hi.is/university/regulation_no_995_2017_on_doctoral_study_at_the_school_of_engineering_and_natural
% - Changed "List of Original Papers" to "List of Original Publications".
% - Removed comma between zipcode and Reykjavík
% - Provided a \thispagestyle{sectionstartwithoutheader} used whenever a new \section starts
% - Removed headline "Dedication" on the dedication page
% - Renamed "Table of Contents" to "Contents"
% - Make table of contents page numbers always right-justified by using "~" instead of "\ ",
%   i.e. change {\ \titlerule*[.5pc]{.}\ \thecontentspage to {\ \titlerule*[.5pc]{.}~\thecontentspage
% - In the table of contents and the list of figures/tables/publications changed running heads to "Contents" and swapped \leftmark and \rightmark
% - Added \usepackage[titles]{tocloft} to makes in the table of contents subsections indented
% - Added \markboth for abbrevations part
% - Added hyperef package

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ui-phdthesis}[30/03/2022, v2.0.0]
\RequirePackage[utf8]{inputenc}
\RequirePackage{t1enc}
\RequirePackage[icelandic,american]{babel}
\RequirePackage{ifthen}
\RequirePackage{calc}
\AtEndOfClass{\RequirePackage{microtype}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions*
\LoadClass[10pt]{article}
\RequirePackage{times}      % Loads the Times-Roman Fonts
\RequirePackage{mathptmx}   % Loads the Times-Roman Math Fonts
\RequirePackage{ifpdf}
%\RequirePackage{showframe}  %show page geometry
\RequirePackage{eso-pic}  %show page geometry
\RequirePackage{amsmath,amsfonts,amssymb}
\RequirePackage{graphicx,xcolor}
\RequirePackage{booktabs}
\RequirePackage{authblk}
\RequirePackage{textcomp}
\RequirePackage{abstract}
\RequirePackage{emptypage}
\RequirePackage{url}
\RequirePackage{enumitem}
%
%
\RequirePackage[twoside,%
                inner=3.0cm,%
                outer=2.5cm,%
                top=2.5cm,%
                bottom=3.0cm,%
                headheight=14.49998pt,%
                footskip=1.5cm,%
                b5paper]{geometry}%
                
\RequirePackage[labelfont={it},%
                labelsep=period,%
                font={it},%
                justification=raggedright]{caption}
%
\RequirePackage{lastpage}  % Number of pages in the document
\RequirePackage{fancyhdr}  % custom headers/footers
\RequirePackage{hyperref} 
%\RequirePackage[hidelinks]{hyperref} % Use if you dont want to have ugly boxes around clickable cross-reference when reading the PDF on-screen
\RequirePackage{bookmark} % To be able to set PDF bookmarks (i.e. navigation in the sidebar of a PDF viewer)
\pagestyle{fancy} % choose the "fancy" pagestyle
%
%
\setlength{\parskip}{0pt}
%

% Packages used for title page layout
\RequirePackage{xcolor}
\RequirePackage{tikz}
\usetikzlibrary{positioning}

% Blue color according to HÍ corporate design
\convertcolorspec{RGB}{16,9,159}{rgb}\tmphiblue
\definecolor{hiblue}{rgb}\tmphiblue
\definecolor{color1}{RGB}{0,0,0} % Color of section headings
%
\renewcommand{\thetable}{\arabic{section}.\arabic{table}}   
\renewcommand{\thefigure}{\arabic{section}.\arabic{figure}}
%
% We automate insertion of pdffiles one page at a time using
% includegraphics
%
\makeatletter
\newcounter{imagepage}
\newcommand*{\foreachpage}[2]{%
  \begingroup
    \sbox0{\includegraphics{#1}}%
    \xdef\foreachpage@num{\the\pdflastximagepages}%
  \endgroup
  \setcounter{imagepage}{0}%
  \@whilenum\value{imagepage}<\foreachpage@num\do{%
    \stepcounter{imagepage}%
    #2\relax
  }%
}
\makeatother
%
\newcommand{\insertpdffile}[7]{%
\newpage
\let\oldleftmark=\leftmark
\let\oldrightmark=\rightmark
\renewcommand{\leftmark}{\footnotesize\sffamily#1}
\renewcommand{\rightmark}{\footnotesize\sffamily#1}
\vspace*{38mm}
%
\noindent\colorbox{hiblue}{\parbox{\textwidth}{\color{white}\hfill\Huge\sffamily\bfseries #1\hspace*{15mm}}} % A blue box with paper ID
\phantomsection
%{\addcontentsline{toc}{section}{#1}} % TODO: Check whether "\addcontentsline{toc}{section}{\hspace*{1.0pt}#1}" would be better
{\addcontentsline{toc}{section}{\hspace*{1.0pt}#1}}
% TODO: Clickable PDF hyperlinks in the ToC are wrong.
\vspace*{0.5ex}

\vspace{1ex}\noindent{\sffamily\bfseries #2} % Paper name
\vspace*{0.5ex}

\noindent#3 %Paper authors
\vspace*{0.5ex}

\noindent#4 % Location of publication
\vspace*{1.5ex}

\noindent#5 % Reprint permission copyright info
\vspace*{1.5ex}

\noindent#6 % Contribution of phd candidate
\clearpage
\foreachpage{#7}{% File name to be included
  \newpage   
  \begingroup 
    \centering
    \includegraphics[
      page=\value{imagepage},
      width=\textwidth,  % TODO: Check whether "width=0.99\textwidth" would be better  
      height=\textheight, % TODO: Check whether "height=0.99\textheight" would be better  
      keepaspectratio,
    ]{#7}%
    \newpage
  \endgroup
}
\let\leftmark=\oldleftmark
\let\rightmark=\oldrightmark
}
%
% headers and footers
\RequirePackage[explicit]{titlesec}
\titleformat{\section}
 {\color{color1}\LARGE\sffamily\bfseries}
 {\thesection}
 {0.5em}
 {#1}
 []
%
\titleformat{\subsection}
  {\Large\sffamily\bfseries}
  {\thesubsection}
  {0.5em}
  {#1}
  []
%
\titleformat{\subsubsection}
  {\sffamily\normalsize\sffamily\bfseries}
  {\thesubsubsection}
  {0.5em}
  {#1}
  []  
%  
\titleformat{\paragraph}[runin]
  {\sffamily\normalsize}
  {}
  {0em}
  {#1} 
%
\renewcommand{\beforetitleunit}{1ex}
\titlespacing*{\section}{0pc}{54pt plus 1pt minus 1pt}{1pc}
\titlespacing*{\subsection}{0pt}{24pt}{10pt}
\titlespacing*{\subsubsection}{0pt}{18pt}{6pt}
\titlespacing*{\paragraph}{0pc}{1.5ex \@plus2pt \@minus1pt}{10pt}
\newcommand{\sectionbreak}{\cleardoublepage\phantomsection\thispagestyle{sectionstartwithoutheader}\null}
%  
%
% tableofcontents set-up
%
\newlength{\tocsep} 
\setlength\tocsep{1.5pc} 
% Sets the indentation of the sections in the table of contents
\newlength{\tocsepsub} 
%\setlength\tocsepsub{3.0pc}
\setlength\tocsepsub{2.0pc} 
% Sets the indentation of the subsections in the table of contents
\newlength{\tocsepsubsub} 
\setlength\tocsepsubsub{4.5pc} 
% Sets the indentation of the subsections in the table of contents
\setcounter{tocdepth}{4} % Show only three levels in the table of contents section: sections, subsections and subsubsections (adjust as needed)
\usepackage{titletoc}
\contentsmargin{0cm}
\titlecontents{section}[\tocsep]
  {\addvspace{4pt}\small\bfseries\sffamily}
  {\contentslabel[\thecontentslabel]{\tocsep}}
  {}
  {\hfill\thecontentspage}
  []
\titlecontents{subsection}[\tocsepsub]
  {\addvspace{2pt}\small\sffamily}
  {\contentslabel[\thecontentslabel]{\tocsepsub}}
  {}
  {\ \titlerule*[.5pc]{.}~\thecontentspage}[] % TODO: Others used her rather  {\ \titlerule*[.25pc]{.}~\thecontentspage}[]
\titlecontents*{subsubsection}[\tocsepsubsub]
  {\footnotesize\sffamily}
  {\contentslabel[\thecontentslabel]{\tocsepsubsub}}
  {}
  {\ \titlerule*[.5pc]{.}~\thecontentspage}
  [\ \textbullet\ ]  

\usepackage[titles]{tocloft} % This makes in the table of contents subsections indented
\cftsetpnumwidth{2em} % This is the horizontal space reserved for the page numbers in the ToC -- the default should be OK

%
%-----------------------------------------------
\let\oldbibliography\thebibliography
\renewcommand{\thebibliography}[1]{%
\addcontentsline{toc}{section}{\hspace*{-\tocsep}\refname}% TODO: Others used here rather \addcontentsline{toc}{section}{\hspace*{1pt}\refname}
\oldbibliography{#1}%
\setlength\itemsep{0pt}%
}
% We need this command if someone used \\ in the thesis title
\newcommand{\removelinebreaks}[1]{%
  \begingroup\def\\{}#1\endgroup}
%%
\makeatletter
\def\thesisauthor#1{\gdef\@thesisauthor{#1}}					
\def\thesistitle#1{\gdef\@thesistitle{#1}} 					
\def\thesisshorttitle#1{\gdef\@thesisshorttitle{#1}}
\def\thesismonth#1{\gdef\@thesismonth{#1}}					
\def\thesisyear#1{\gdef\@thesisyear{#1}}
\def\thesisdegree#1{\gdef\@thesisdegree{#1}}
\def\thesisfield#1{\gdef\@thesisfield{#1}}
\def\thesisschool#1{\gdef\@thesisschool{#1}}
\def\thesisfaculty#1{\gdef\@thesisfaculty{#1}}					
\def\thesisaddress#1{\gdef\@thesisaddress{#1}}
\def\thesispostalcode#1{\gdef\@thesispostalcode{#1}}
\def\thesistelephone#1{\gdef\@thesistelephone{#1}}				
\def\thesisprinter#1{\gdef\@thesisprinter{#1}}					
\def\thesisadvisor#1{\gdef\@thesisadvisor{#1}}
\def\thesiscommittee#1{\gdef\@thesiscommittee{#1}}
\def\thesisopponents#1{\gdef\@thesisopponents{#1}}
\def\thesiskeywords#1{\gdef\@thesiskeywords{#1}}
\def\thesisISBN#1{\gdef\@thesisISBN{#1}}
\def\thesislicense#1{\gdef\@thesislicense{#1}}
\makeatother
%
\makeatletter
\newcommand\BackgroundPic{
\put(0,0){
\parbox[b][\paperheight]{\paperwidth}{
\vfill
\centering
\hspace*{-0.6cm}
\includegraphics[width=2\paperwidth,height=\paperheight,
keepaspectratio]{UIblueribbon}
}}
\setlength{\unitlength}{\paperwidth}
\begin{picture}(0,0)(0,-0.15)
\put(0,0){\color{white}\parbox{1\paperwidth}{\centering\bfseries\sffamily
    \Large Faculty of \@thesisfaculty \\ 
University of Iceland\\
\@thesisyear}}
\end{picture}
}
\makeatother

\makeatletter
\def\makecovertitlepage{
\thispdfpagelabel{Cover}
\pagenumbering{Alph} % To prevent warnings from hyperref that an internal PDF page numer is already used, we need to set a pagenumbering here that is used nowhere else
  % The banner at top and bottom (using a tikz overlay)
  \begin{tikzpicture}[remember picture,overlay]
    \node[anchor=north west, inner sep=0pt] at (current page.north west)
        {\includegraphics[width=\paperwidth]{banner}}; % The top banner (as a PNG) % TODO: A vector graphic would be better

    \node(bottom)[shape=rectangle, fill=hiblue, minimum height=10mm, minimum width=\paperwidth, anchor=south west] at (current page.south west) {}; % The bottom banner (a filled rectangle)

    \node[above=0.4cm of bottom] {
        \begin{tabular}{c} 
          \sffamily \small \textcolor{hiblue}{\textbf{\MakeUppercase{Faculty of \@thesisfaculty{}}}}
        \end{tabular}
    };
  \end{tikzpicture}

  \enlargethispage{3cm}
  \vspace*{2.9cm} % Here starts the white space below the top banner
  
  % The centering used below is with respect to the page margins which
  % are not the same on left and right which prevents proper centering
  % with respect to the tikz centering (and the title page in
  % general).  Instead, we use a minipage that we shift horizontally
  % by 2.6 cm.  But minipage sets \parskip to 0, so we need to save
  % and restore it.  To be able to use vfill/stretch in a minipage,
  % the height needs to be specified: 20.0 cm.
  \newlength{\currentparskip}
  \setlength{\currentparskip}{\parskip}
  \hspace*{-3.575cm}
  \begin{minipage}[t][16.7cm]{1.0\paperwidth}
    \setlength{\parskip}{\currentparskip}
    \begin{center}
      \vspace*{ \stretch{1.5} }
      {\bfseries\sffamily\Large{\@thesistitle}\par}
%    
%      \normalfont \LARGE \sffamily \thesissubtitle{} %% TODO: Support thesis subtitle (as in the MSc thesis template)
%
      \vspace{ \stretch{2.5} }
      {\sffamily\Large{\@thesisauthor}\par}
%
      \vspace*{ \stretch{2.75} }
%
      {\sffamily\Large{\@thesisyear}\par}
%
      \vspace*{ \stretch{0.5} }
%    
      \vspace*{ \stretch{1.0} }
    \end{center}
  \end{minipage}

\thispagestyle{empty}
\cleardoublepage
}
\makeatother

\makeatletter
\def\makeinnertitlepage{% 
  {\setlength{\parskip}{0pt}
  \thispagestyle{empty}
  \setcounter{page}{1}
  \pagenumbering{roman}
  \cfoot{\thepage}
  \vspace*{\fill}
  {\centering\bfseries\sffamily\LARGE{\@thesistitle} \par}
  \vspace{ \stretch{1.6} }
  {\centering\sffamily\Large {\@thesisauthor}\par}
  \vspace{ \stretch{1.6} }
  {\centering\sffamily\large Dissertation submitted in partial fulfillment of a \par
  \sffamily\itshape\large\@thesisdegree{} degree in {\@thesisfield}\par}
  \vspace{ \stretch{1.4} }
  {\centering\sffamily\large Supervisor\\ {\@thesisadvisor}\par}
  \vspace{ \stretch{1.0} }
  {\centering\sffamily\large Doctoral Committee\\{\@thesiscommittee}\par}
  \vspace{ \stretch{1.0} }
  {\centering\sffamily\large Opponents\\ {\@thesisopponents}\par}
  \vspace{ \stretch{1.0} }
  {\centering\sffamily\large Faculty of {\@thesisfaculty}\\}
  {\centering\sffamily\large {\@thesisschool}\\}
  {\centering\sffamily\large University of Iceland\\}
  {\centering\sffamily\large Reykjavik,
    {\@thesismonth}~{\@thesisyear}\\}
}
\clearpage
}
\makeatother
%
\makeatletter
\def\makesaurblad{% This is the title verso (colophon), i.e. imprint/copyright page

{\scriptsize
  {\setlength{\parskip}{0cm plus0mm minus0mm}
  \thispagestyle{empty}
  \vspace*{\fill}
{\noindent\removelinebreaks{\@thesistitle}\\}
%
\ifx\@thesisshorttitle\empty % Show only if shorttitle is provided
\else
{(\noindent\@thesisshorttitle{})\\}
\fi

\noindent Dissertation submitted in partial fulfillment of a {\itshape
  Philosophiae Doctor} degree in {\@thesisfield} \\ \\

\noindent Faculty of {\@thesisfaculty\\}
\noindent {\@thesisschool\\}
\noindent University of Iceland\\
\noindent{\@thesisaddress\\}
\noindent{\@thesispostalcode} Reykjavik\\
\noindent Iceland\\ \\

\noindent Telephone: {\@thesistelephone} \\ \\ 
  \vspace*{10\lineskip}

\noindent Bibliographic information:\\
\noindent{\@thesisauthor{}  (\@thesisyear) {\itshape \removelinebreaks{\@thesistitle}}, PhD
  dissertation, Faculty of {\@thesisfaculty}, University of Iceland,
  \pageref{LastPage}~pp.\\ \\
}

\noindent ISBN~{\@thesisISBN} \\ \\


\noindent Copyright \textcopyright~{\@thesisyear~\@thesisauthor\\}
\noindent {\@thesislicense} \\ \\

\noindent Printing: {\@thesisprinter\\}
\noindent Reykjavik, Iceland, {\@thesismonth~\@thesisyear\par}
}
\clearpage
}}
\makeatother
\selectlanguage{american}
%
\renewcommand{\abstractname}{Abstract}
\newcommand\thesisabstract[1]{%
\thispagestyle{empty}
\section*{Abstract}
\phantomsection
\addcontentsline{toc}{section}{Abstract}
{#1}
\cleardoublepage
}
%
\newcommand\thesisutdrattur[1]{%
\renewcommand{\abstractname}{Útdráttur}
\thispagestyle{empty}
\section*{Útdráttur}
\phantomsection
\addcontentsline{toc}{section}{Útdráttur}
{\selectlanguage{icelandic}#1}
\cleardoublepage
}
%
\newcommand\thesispreface[2]{%
{\ifthenelse{\equal{#1}{TRUE}}{%
\thispagestyle{empty}
\section*{Preface}
\phantomsection
\addcontentsline{toc}{section}{Preface}
#2
\cleardoublepage
}{}
}
{\ifthenelse{\equal{#1}{FALSE}}{}{}}
}
%
\newcommand\thesisdedication[2]{%
{\ifthenelse{\equal{#1}{TRUE}}{%
\thispagestyle{empty}
\phantomsection
\addcontentsline{toc}{section}{Dedication}
\vspace*{50mm}
\begin{center}
  %  {\normalfont\itshape Dedication\\\vspace*{2ex}#2} % Typically, there is just the dedication sentence, but not a headline
    {\normalfont\itshape #2}
\end{center}
\cleardoublepage
}{}
}
{\ifthenelse{\equal{#1}{FALSE}}{}{}}
}
%
\newcommand{\abbreviationsname}{Abbreviations} % Change as appropriate, e.g. "Acronyms"
\newcommand\thesisabbreviations[2]{%
{\ifthenelse{\equal{#1}{TRUE}}{%
\markboth{}{\normalfont\footnotesize\sffamily{\abbreviationsname}} % Needed if the Abbreviations span multiple pages.
\section*{\abbreviationsname}\thispagestyle{sectionstartwithoutheader}
\phantomsection
\addcontentsline{toc}{section}{\abbreviationsname}
  {#2}
\renewcommand{\rightmark}{\normalfont\footnotesize\sffamily\nouppercase{\abbreviationsname}}
\renewcommand{\leftmark}{}

\cleardoublepage
}{}
}
{\ifthenelse{\equal{#1}{FALSE}}{}{}}
}
%
\newcommand\thesisacknowledgments[2]{%
{\ifthenelse{\equal{#1}{TRUE}}{%
\section*{Acknowledgments}\thispagestyle{sectionstartwithoutheader}
\phantomsection
\addcontentsline{toc}{section}{Acknowledgments}
  {#2}
\renewcommand{\leftmark}{\normalfont\footnotesize\sffamily\nouppercase{Acknowledgments}}
\renewcommand{\rightmark}{}
\cleardoublepage
}{}
}
{\ifthenelse{\equal{#1}{FALSE}}{}{}}
}
%
\newcommand\thesislists{%
\let\oldleftmark=\leftmark
\let\oldrightmark=\rightmark
\renewcommand{\contentsname}{Contents} % Might also use "Table of Contents" -- but this is less common
%\thispagestyle{empty}
\thispagestyle{sectionstartwithoutheader}
\phantomsection
\addcontentsline{toc}{section}{\contentsname}
\renewcommand{\rightmark}{\normalfont\footnotesize\sffamily\contentsname}
\renewcommand{\leftmark}{}
\tableofcontents
\cleardoublepage
\phantomsection
\addcontentsline{toc}{section}{\listfigurename}
\renewcommand{\rightmark}{\normalfont\footnotesize\sffamily\nouppercase\listfigurename}
\renewcommand{\leftmark}{}
\thispagestyle{sectionstartwithoutheader}
\listoffigures
\cleardoublepage
\thispagestyle{sectionstartwithoutheader}
\listoftables
\phantomsection
\addcontentsline{toc}{section}{\listtablename}
\renewcommand{\rightmark}{\normalfont\footnotesize\sffamily\nouppercase\listtablename}
\renewcommand{\leftmark}{}
\cleardoublepage
\let\leftmark=\oldleftmark
\let\rightmark=\oldrightmark
}
%
\newcommand\thesisbody{%
\fancyhf{}        % clear all headers and footers
\pagenumbering{arabic}
% Now set the headers
%\fancyhead[LE,RO]{\footnotesize\thepage}
\fancyhead[LE]{\normalfont\footnotesize\sffamily\nouppercase{\leftmark}}
\fancyhead[RO]{\normalfont\footnotesize\sffamily\nouppercase{\rightmark}}
%\fancyhead{}
\fancyfoot[LE]{\thepage}%
\fancyfoot[RO]{\thepage}%
\pagestyle{fancy} % Enables the custom headers/footers
% % Headers
% \lhead{}%
% \chead{}%
% \rhead{}%
% % Footers
%\lfoot{}%
\renewcommand{\cfoot}{}%
% \rfoot{}%
}
%
%
\fancypagestyle{sectionstartwithoutheader}{% Use \section{XZY}\thispagestyle{sectionstartwithoutheader} to skip the header on start page of a section
    \fancyhead{}
    \renewcommand{\headrulewidth}{0pt}
}
\newenvironment{mypaper}[2]{%
\noindent
  \begin{tabular}{p{12ex}p{0.75\textwidth}}
  \textbf{#1:}& {#2}\\
  \end{tabular}
\par
}
{}

\newcommand{\listofpapersname}{List of Original Publications} % Change as appropriate
%
\newcommand\thesislistofpapers[2]{%
{\ifthenelse{\equal{#1}{TRUE}}{%
\section*{\listofpapersname}\thispagestyle{sectionstartwithoutheader}
\phantomsection
\addcontentsline{toc}{section}{\listofpapersname}
  {#2}
\renewcommand{\rightmark}{\normalfont\footnotesize\sffamily\nouppercase{\listofpapersname}} 
\renewcommand{\leftmark}{}
% Caveat: Commenting \cleardoublepage messes up the list of publications
\cleardoublepage
}{}
}
{\ifthenelse{\equal{#1}{FALSE}}{}{}}
}
